<!DOCTYPE HTML>
<html lang="es">
	<?php $helper->head(); ?>
	<body data-template="<?php echo $templateName; ?>">
		<?php 
			$helper->menu();
			$helper->render($view, $helper, $datos);
			$helper->footer();
		?>
	</body>
</html>
