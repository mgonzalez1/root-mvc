<?php

namespace App\Controllers;

use Core\ControladorBase;
use App\Models\Usuario;
use App\Models\Producto;

class UsuariosController extends ControladorBase {

    function __construct() {
        parent::__construct();
    }
    
    public function index() {

        //Creamos el objeto usuario
        $usuario = new Usuario($this->adapter);

        //Conseguimos todos los usuarios
        $allusers = $usuario->getAll();

        //Producto
        $producto = new Producto($this->adapter);
        $allproducts = $producto->getAll();

        //Cargamos la vista index y le pasamos valores
        $this->view("usuarios", "home", array(
            "allusers" => $allusers,
            "allproducts" => $allproducts,
            "Hola" => "Soy Víctor Robles"
        ));
    }

    public function crear($request) {
        if (isset($request["nombre"])) {

            //Creamos un usuario
            $usuario = new Usuario($this->adapter);
            $usuario->setNombre($request["nombre"]);
            $usuario->setApellido($request["apellido"]);
            $usuario->setEmail($request["email"]);
            $usuario->setPassword(sha1($request["password"]));
            $save = $usuario->save();
        }
        $this->redirect("usuarios");
    }

    public function borrar($request) {
        if (isset($request["id"])) {
            $id = (int) $request["id"];

            $usuario = new Usuario($this->adapter);
            $usuario->deleteById($id);
        }
        $this->redirect('usuarios');
    }

}
